#source files
SRC = Logfile.cpp

OBJ = $(SRC:.cpp=.o)

OUT = Logfile.a

LOGOBJ = Logfile.o

#include directories
INCLUDES = -l. -lC:\WindRiver\vxworks-6.3\target\h -lC:\WindRiver\vxworks-6.3\target\h\WPIlib

#C++ compiler flags
CCFLAGS = -g

#Compiler
CCC = ccppc

#Library paths
LIBS = -LC:\workspace\Logfile -lm

#Compile Flags
LDFLAGS = -g
.SUFFIXES: .cpp

default: $(OUT)

.cpp.o:
	$(CCC) $(INCLUDES) $(CCFLAGS) -c $< -o $@
$(OUT):$(OBJ)
	arppc rcs $(OUT) $(OBJ)
clean:
	rm -f $(LOGOBJ) $(OUT)
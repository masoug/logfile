/**
 * @file Logfile.cpp
 * @author 2010 Los Altos High Schoool Robotics Team
 * @version 1.0
 *
 * @section LICENSE This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.  This program is
 * distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.  You should have received a copy of the GNU General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION A C++ logfile library for debugging robot.
 *
 */

#include <iostream>
#include <stdio.h>
#include <time.h>
#include "Logfile.h"

using namespace std;

/**
 * Opens the logfile for appending, or creates one if it does not exist.
 */
LogFile::LogFile() {
        log = fopen("log.txt", "a");
        fclose(log);
}

/**
 * Deconstructor to deallocate pointer created by Logfile::LogFile().
 */
LogFile::~LogFile() {
        delete log;
}

/**
 * Behaves like fprintf(), writing to the logfile.
 * @param print Whether to print the message to stdout.
 * @param information Format string.
 * @param ... Optional format specifiers.
 */
void LogFile::write(bool print, const char* information, ...) {
        log = fopen("log.txt", "a");
        va_list args;
        va_start(args, information);
        fprintf(log, information, args);
        if (print)
                printf(information, args);
        va_end(args);
        fclose(log);
}

/**
 * Starts and populates a new entry in the logfile, including a timestamp.
 * @param print Whether to print the message to stdout.
 * @param anytime Whether to include a timestamp.
 * @param begin Literal string.
 */
void LogFile::newEntry(bool print, bool anytime, const char* begin) {
        log = fopen("log.txt", "a");
        if (anytime) {
                time_t rawtime;
                struct tm * timeinfo;
                time(&rawtime);
                timeinfo = localtime(&rawtime);
                fprintf(log, "\n\n%s on: %s\n", begin, asctime(timeinfo));
                if (print)
                        printf("%s", begin);
        } else {
                fprintf(log, "\n\n%s\n", begin);
                if (print)
                        printf("%s", begin);
        }
        fclose(log);
}

/**
 * Ends an entry in the logfile with a visual delimiter.
 */
void LogFile::endEntry() {
        log = fopen("log.txt", "a");
        fprintf(log, "\n\n==================================\n");
        fclose(log);
}

/**
 * Prints a status string for devices to the logfile.
 * @param print Whether to print the message to stdout.
 * @param success Whether initialization was successful.
 * @param device Device that successfully initialized.
 */
void LogFile::status(bool print, bool success, const char* device) {
        log = fopen("log.txt", "a");
        const char* sstring = "Success: %s okay.\n";
        const char* fstring = "Error: %s did not initiate properly.\n";
        if (success) {
                fprintf(log, sstring, device);
                if (print)
                        printf(sstring, device);
        } else {
                fprintf(log, fstring, device);
                if (print)
                        printf(fstring, device);
        }
        fclose(log);
}
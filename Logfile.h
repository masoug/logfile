/**
 * @file Logfile.h
 * @author 2010 Los Altos High Schoool Robotics Team
 * @version 1.0
 *
 * @section LICENSE This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.  This program is
 * distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.  You should have received a copy of the GNU General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION Logfile library header file.
 *
 */

#ifndef LOG_H
#define LOG_H

/**
 * @class Logfile Logfile class.
 */
class LogFile {
    FILE *log;
  public:
    LogFile ();
    ~LogFile();
    void write (bool, const char*, ...);
    void newEntry (bool, bool, const char*);
    void endEntry ();
    void status (bool, bool, const char*);
};

#endif